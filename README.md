## devcss
> This will simplify your .css file 

> At this moment, Only available css buttons. but in future we'll add more(footers, headings, paragraph, navigation many more...)

[![GitHub issues](https://img.shields.io/github/issues/K-136/devcss)](https://github.com/K-136/devcss/issues)
[![GitHub license](https://img.shields.io/github/license/K-136/devcss?color=skyblue)](https://github.com/K-136/drvcss/blob/master/LICENSE)
![GitHub repo size](https://img.shields.io/github/repo-size/k-136/devcss)

![GitHub top language](https://img.shields.io/github/languages/top/k-136/devCss)
![GitHub last commit](https://img.shields.io/github/last-commit/k-136/devCss)
![GitHub contributors](https://img.shields.io/github/contributors/k-136/devCss?color=blue)

Just another small noob project by me([K-136](https://github.com/K-136)).

### Support me

<a href="https://www.buymeacoffee.com/Mizuhara" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height:40px" height="40px" ></a>

### Why this

**Mm.. I'm just trying to improve my css skill and share it with you :)**
### How to use

Paste this code on your head section 

```html
<link rel="stylesheet" href="https://cdn-dev.pages.dev/css/dev.css">
```

or 

Import to your css file 

```css
@import url(https://cdn-dev.pages.dev/css/dev.css);
```
Yay🎉.! Now you can use our buttons

Use this template for create button

##### Just a normal link

```html
<button onclick="location.href='yourlink'" type="button" class="coffee"></button>
```
###### Open on new window

```html
<button onclick="window.open('yourlink','_blank')" class="coffee"></button>
```

### Note

- Replace your url with ```yourlink```


If you need button with Facebook icon , Just Change class name to facebook. If you need button with Twitter icon Change class name to twitter.(Case sensitive. so use lower case).

Theres still few bugs. I'm fixing it🌝.

Also you can change ```border-radius``` as your need . I didn't include border radius on css code(Maybe).. so you have to do it your self.
Thats it.

###### Now only available small amount of buttons :( . I'm working on this. I'll adding buttons.

You can request buttons via

[![Discord](https://img.shields.io/discord/866845224470052884)](https://discord.gg/KJcquwz4Wx)
[![Join](https://img.shields.io/badge/WalkersCloud%20Chat-black?style=social&logo=Discord)](https://discord.gg/KJcquwz4Wx)
